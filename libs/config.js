const _ = require('lodash')
const path = require('path')
const nconf = require('nconf')
const { NODE_ENV } = process.env

let prefix = ''

switch (NODE_ENV) {
  case 'test':
  case 'staging':
  case 'production':
  case 'development':
    prefix = NODE_ENV
    break
  default:
    prefix = 'development'
}

let file = _.compact([ prefix, 'json' ])

file = file.join('.')
file =  path.join(__dirname, '/../config/', file)

nconf.use('file', { file })

module.exports = nconf
