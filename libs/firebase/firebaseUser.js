var firebase = require('firebase')
const config = require("../config")
var firebaseConfig = {
    apiKey: config.get('firebase:apiKey'),
    authDomain: config.get('firebase:authDomain'),
    databaseURL: config.get('firebase:databaseURL'),
    projectId: config.get('firebase:projectId'),
    storageBucket: config.get('firebase:storageBucket'),
    messagingSenderId: config.get('firebase:messagingSenderId'),
  };
  
firebase.initializeApp(firebaseConfig);

module.exports = { firebase }