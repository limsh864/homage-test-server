const admin = require("firebase-admin");
const config = require("../config")
const serviceAccountKey = require("./newagent-3e58f-c08e2edba9ff.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccountKey),
  databaseURL: config.get('firebase:databaseURL')
});

const firebaseAuth = admin.auth()

module.exports = { firebaseAuth }