let mongoose = require('mongoose');
const Config = require('./config')

const mongoDbConfig = Config.get('mongodb')
class Database {

  constructor() {
    this._connect()
  }

_connect() {
     mongoose.connect(`mongodb://${mongoDbConfig.server}/${mongoDbConfig.database}`)
       .then(() => {
         console.log('Database connection successful')
       })
       .catch(err => {
         console.error('Database connection error')
       })
  }
}
module.exports = new Database()