const Config = require('./config')
const Sequelize = require('sequelize')

const mysqlConfig = Config.get('mysql')

const sequelize = new Sequelize(
  mysqlConfig.db,
  mysqlConfig.user,
  mysqlConfig.password,
  mysqlConfig.options
)

module.exports = sequelize
