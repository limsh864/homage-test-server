const _ = require('lodash')
const co = require('co')
const util = require('util')
const moment = require('moment')

const { singular } = require('pluralize')
const sequelize = require('../models/sequelize')
const packageJson = require('../package')
const startTime = new Date

function _buildInclude(include) {
  if (_.isArray(include)) {
    return include
  }

  return _.map(include, (v, k) => {
    const key = k.charAt(0).toUpperCase() + k.slice(1)
    const keySingular = singular(key)

    // check for both singular and plural table names
    const model = sequelize[key] || sequelize[keySingular]

    const inc = { model, as: k }

    if (_.isObject(v)) {
      inc.include = _buildInclude(v)
    }

    return inc
  })
}

function replaceModel(includeRaw) {
  return _.map(includeRaw, (include, index)=>{
    const modelName = include.model
    const model = sequelize[modelName]
    include.model =  model
    include.as = modelName.charAt(0).toLowerCase()+ modelName.slice(1)
    return include
  })
}

function getEntitySize(Schema, options) {
  options = _.merge(options, { count: true })

  return getEntities(Schema, options)
}

/**
 * @description Sequelize include builder, abstract database dao auto filter with data ownership based on options
 * @param {Sequelize entity} Schema
 * @return {Promise<[schema]>} 
 */
function getEntities(Schema, options) {
  options = options || {}

  const tableName = Schema.getTableName().toLowerCase()

  return co(function * () {

    let queryBuilder = { where: {} }

    const {
      get = '',
      where = {},
      between = {},
      attributes,
      include,
      includeRaw = {},
      group,
      skip,
      limit,
      order,
      raw,
      logging,
      page,
      acceptanceLimit,
      count = false
    } = options

    if (!_.isEmpty(get) && _.isString(get)) {
      queryBuilder.where = _.assign(queryBuilder.where, { id: get })
    } else if (!_.isEmpty(where)) {
      queryBuilder.where = where 
    }

    // TODO: remove this when `where` filter is added
    if (!_.isEmpty(between)) {
      let { start, end } = between

      if (start && end) {
        start = moment(start).utc().toDate()
        end = moment(end).utc().toDate()

        queryBuilder.where.endTime = { $between: [ start, end ] }
      }
    }

    if (_.isArray(attributes)) {
      queryBuilder.attributes = attributes
    }

    if (!_.isEmpty(include)) {
      queryBuilder.include = _buildInclude(include)
    }
    else if(!_.isEmpty(includeRaw)){
      queryBuilder.include = replaceModel(includeRaw)
    }

    if (_.isArray(group)) {
      queryBuilder.group = group
    }

    if (_.isNumber(limit)) {
      queryBuilder.limit = limit
    } else {
      queryBuilder.limit = 20
    }

    if(page){
      if(page > 1)
      {
        queryBuilder.offset = (page - 1) * queryBuilder.limit
      } else {
        queryBuilder.offset = 0
      }
    }

    if (_.isArray(order) || _.isString(order)) {
      queryBuilder.order = order
    }

    if (_.isBoolean(raw)) {
      queryBuilder.raw = raw
    }

    if (logging) {
      queryBuilder.logging = logging
    }

    let result = null
    if(!_.isEmpty(get)){
      result = Schema.findOne(queryBuilder)
    }
    else if (count) {
      result = Schema.count(queryBuilder)
    }
    else{
      result = Schema.findAll(queryBuilder)
    }
    const asyncResult = yield result
    if(acceptanceLimit && _.size(asyncResult) > acceptanceLimit){
      const error =  new Error(`result is more than acceptance limit (${acceptanceLimit})`)
      error.status = 403
      throw error
    }
    
    return asyncResult
  })
}

function uptimeRoute(req, res, next) {
  const ms = moment(new Date).diff(startTime)
  const d = moment.duration(ms)

  let hours = Math.floor(d.asHours())

  if (hours < 10) {
    hours = '0' + hours
  }

  const uptime = hours + moment.utc(ms).format(":mm:ss")

  res.send({
    name: 'Homage Test API',
    version: `v${packageJson.version}`,
    uptime
  })
}

function handler(error) {
  if (process.env.NODE_ENV === 'test') {
    // skip
    return
  }

  console.error(error.stack)
  console.error(util.inspect(error))
}

function resHandler(app, status) {
  return (error, req, res, next) => {
    const ret = {message: error.message, error}

    // production error handler
    // will not print stacktrace
    if (app.get('env') !== 'development') {
      ret.error = {}
    }

    handler(error)

    res.status(error.status || status)
    res.send(ret)
  }
}

function error406(next) {
  return () => {
    const error = new Error('Not Acceptable')
    error.status = 406

    next(error)
  }
}

process.on('unhandledRejection', (error, p) => {
  console.error('Unhandled Rejection', p)
  handler(error)
})

module.exports = {
   getEntitySize, getEntities, uptimeRoute, handler, resHandler, error406
}
