const _ = require('lodash')
const co = require('co')
const { firebase } = require('./firebase/firebaseUser')
const sequelize = require('../models/sequelize')
const { CareOwner, CareProfessional } = sequelize

function login(data) {
  return co(function * () { 
    let { email, password } = data 
    yield firebase.auth().signInWithEmailAndPassword(email,password)
    return { user : firebase.auth().currentUser }
  })
}

function getCareOwnerProfile(uid) {

  return co(function * () {
    let careOwner = yield CareOwner.findOne({ where : { id : uid } })
    careOwner = careOwner.dataValues
    return careOwner
  }).catch(err => {
    console.error(err)
    return null
  })
}

function getCareProfessionalProfile(uid) {

  return co(function * () {
    let careProfessional = yield CareProfessional.findOne({ where : { id : uid } })
    careProfessional = careProfessional.dataValues
    return careProfessional
  }).catch(err => {
    console.error(err)
    return null
  })
}


module.exports = {
  login, getCareOwnerProfile, getCareProfessionalProfile
}
