const co = require('co')
const _ = require('lodash')
const express = require('express')
const moment = require('moment')
const { getEntities, error406 } = require('../libs/common')
const { CareProfessional, CareRecipient } = require('../models/sequelize')
let VisitModel = require('../mongodb/Visit')
const router = express.Router()

router.get(`/visits`, (req, res, next) => {
  res.format({
    default: error406(next),
    json: () => co(function * () {
      let query = VisitModel.
        find().
        where('careOwnerId').equals('r6sUJmsXuMgEGGWAe6mucyTBAOr2').sort({ visitStartTime : 'desc'});
      let result = yield query.exec()
      result = yield _.map(result, co.wrap(function * (visit){
        let { careProfessionalId, careRecipientId } = visit
        let careProfessionalObj = yield CareProfessional.findOne({ where : { id : careProfessionalId }})
        let careRecipientObj = yield CareRecipient.findOne({ where : { id : careRecipientId}})
        visit.careProfessional = careProfessionalObj ? careProfessionalObj.dataValues : null
        visit.careRecipient = careRecipientObj ? careRecipientObj.dataValues : null 
        delete visit.careRecipientId
        delete visit.careProfessionalId
        return visit
      }))
      res.send(result)
    })
    .catch((error) => {
      console.error('@error', error)

      if (!error.status) {
        error = new Error(`Unable to get available feed for merchant ${req.user.merchantName}`)
      }

      next(error)
    })
  })
})

module.exports = router


