const _ = require('lodash')
const fs = require('fs')

const routeNames = fs.readdirSync(__dirname).filter((file) => {
  // include only js files in current directory,
  // exclude index.js
  return /^(.+)\.js$/.test(file) && file !== 'index.js'
}).map((file) => file.slice(0, -3))

const routes = {}

_.each(routeNames, (name) => {
  let key = _.kebabCase(name)
  key = key.replace('-v-', '-v')
  routes[key] = require(`./${name}`)
})

module.exports = routes
