const co = require('co')
const _ = require('lodash')
const express = require('express')
const { error406 } = require('../libs/common')
let VisitModel = require('../mongodb/Visit')
const router = express.Router()

router.get('/', (req, res, next) => {
  const { user } = res
  const { id } = user
  co(function * () {
      let query = VisitModel.
        find().
        where('careProfessionalId').equals('VPMBOcF5bDY4yPwvPexYYNsz1gV2').sort({ visitStartTime : 'desc'});
      let result = yield query.exec()
      res.send({ visits : result })
  })
  .catch((error) => {
    error.status = 403
    next(error)
  })
})
.put(`/clock-in/:visitId`, (req, res, next) => {
  let { visitId } = req.params
  let { body } = req
  let { clockInTime } = body
  res.format({
      default: error406(next),
      json: () => co(function * () {
        // let result = yield VisitModel.findById(visitId).exec()
        VisitModel.findOne({ _id : visitId }, function (err, doc){
          doc.clockInTime = clockInTime
          doc.save();
          res.send({ doc })
        });
          
      })
      .catch((error) => {
          console.error('@error', error)

          if (!error.status) {
              error = new Error(`Unable to update profile`)
          }
          next(error)
      })
  })
})
.put(`/clock-out/:visitId`, (req, res, next) => {
  let { visitId } = req.params
  let { body } = req
  let { clockOutTime } = body
  res.format({
      default: error406(next),
      json: () => co(function * () {
        // let result = yield VisitModel.findById(visitId).exec()
        VisitModel.findOne({ _id : visitId }, function (err, doc){
          doc.clockOutTime = clockOutTime
          doc.save();
          res.send({ doc })
        });
      })
      .catch((error) => {
          console.error('@error', error)

          if (!error.status) {
              error = new Error(`Unable to update profile`)
          }
          next(error)
      })
  })
})
.put(`/summary/:visitId`, (req, res, next) => {
  let { visitId } = req.params
  let { body } = req
  let { visitSummary } = body
  res.format({
      default: error406(next),
      json: () => co(function * () {
        // let result = yield VisitModel.findById(visitId).exec()
        VisitModel.findOne({ _id : visitId }, function (err, doc){
          doc.visitSummary = visitSummary
          doc.save();
          res.send({ doc })
        });
      })
      .catch((error) => {
          console.error('@error', error)

          if (!error.status) {
              error = new Error(`Unable to update profile`)
          }
          next(error)
      })
  })
})

module.exports = router
