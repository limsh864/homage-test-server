const co = require('co')
const _ = require('lodash')
const express = require('express')
const Auth = require('../libs/auth')

const router = express.Router()

router.post('/owner-login', (req, res, next) => {
  const { body } = req
  const { email, password } = body
  const credentials = { email, password }

  co(function * () {
    let { user } = yield Auth.login(credentials)
    const token = yield user.getIdToken()
    res.send({ token, message : 'Login Success', status : 'success', type : 'owner'})
  })
  .catch((error) => {
    error.status = 403
    next(error)
  })
})
.get('/logout', (req, res) => {
  req.logout()
  res.sendStatus(200)
})

module.exports = router
