const _ = require('lodash');
const co = require('co')
const { firebaseAuth } = require('../libs/firebase/firebaseAdmin');
const { getCareOwnerProfile,getCareProfessionalProfile } = require('../libs/auth')

function careProfessionalTokenVerifier(req, res, next) {
  co(function * () {
    const { authorization } = req.headers;
    if(!authorization){
      return res.status(401).send(new Error('Undefined id token'))
    }
    let decodedToken = yield firebaseAuth.verifyIdToken(authorization)
    if(decodedToken) {
      let { uid } = decodedToken
      const user = yield getCareProfessionalProfile(uid);
      if (_.size(user)) {
        req.user = user;
        next();
      }
    }
  }).catch(function(error) {
    console.error('@CareProfessionalTokenVerifyError - ', error)
  })
} 

function careOwnerTokenVerifier(req, res, next) {
  co(function * () {
    const { authorization } = req.headers;
    if(!authorization){
      return res.status(401).send(new Error('Undefined id token'))
    }
    
    let decodedToken = yield firebaseAuth.verifyIdToken(authorization)
    if(decodedToken) {
      let { uid } = decodedToken
      const user = yield getCareOwnerProfile(uid);
      if (_.size(user)) {
        req.user = user;
        next();
      }
    }
  }).catch(function(error) {
    console.error('@CareOwnerTokenVerifyError - ', error)
  })
} 

module.exports = { careProfessionalTokenVerifier, careOwnerTokenVerifier }