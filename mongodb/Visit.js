const mongoose = require('mongoose')

let visitSchema = new mongoose.Schema({
    visitId : mongoose.Schema.ObjectId,
    careProfessionalId : String,
    careRecipientId : String,
    careOwnerId : String,
    visitStartTime : Number,
    visitEndTime : Number,
    clockInTime : Number,
    clockOutTime : Number,
    location: {
      address : String,
      coordinates: { type: [Number] }
    },
    visitSummary : {
      bloodPressureSummary : [{ 
        time : Number,
        systolicReading : Number,
        diastolicReading : Number
      }],
      heartRateSummary : [{
        time : Number,
        heartRateCount : Number,
      }],
      alertness : String,
      extraComments : String
    }
  })
  
module.exports = mongoose.model('Visit', visitSchema, 'Visit')