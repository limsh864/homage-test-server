
const _ = require('lodash')
const express = require('express')
const compression = require('compression')
const cors = require('cors')
const logger = require('morgan')
const bodyParser = require('body-parser')
const boolParser = require('express-query-boolean')
const Common = require('./libs/common')
const Config = require('./libs/config')
const routes = require('./routes')
let mongoose = require('mongoose')
const mongoDbConfig = Config.get('mongodb')
const { careOwnerTokenVerifier, careProfessionalTokenVerifier } = require('./middleware/firebase')
const app = express()

app.use(logger('dev'))
app.use(compression({ threshold: 0 }))
app.use(bodyParser.json({ limit: '10mb' }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: false }))
app.use(boolParser())


var mongoDB = `mongodb://${mongoDbConfig.server}/${mongoDbConfig.database}`;
mongoose.connect(mongoDB, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// default route - server uptime, app version check (no security needed)
app.get('/', Common.uptimeRoute)

_.each(routes, (module, name) => {
  const route = `/${name.toLowerCase()}`

  if ( name.indexOf('auth') > -1 )
  {
    app.use(route, (req, res, next) => {
      console.log('No Auth API : ' + name)
      next()
    }, module)
  } else if (name.indexOf('professional') > -1){
    app.use(route, (req, res, next) => {
      console.log('Care Professional API : ' + name)
      next()
    }, module)
  } else if (name.indexOf('owner') > -1){
    app.use(route, (req, res, next) => {
      console.log('Care Owner API : ' + name)
      next()
    }, module)
  }
  console.log(`Setting up route ${route} ...`)
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404

  next(err)
})

// error handlers
app.use(Common.resHandler(app, 500))

module.exports = app
