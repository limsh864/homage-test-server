const Sequelize = require('sequelize')
const sequelize = require('../../libs/sequelize')

const Schedule = sequelize.define('Schedule', {

  visitId : { type: Sequelize.UUID, allowNull: false },
}, {
  paranoid: true
})

function initialize(model) {
    return Schedule
}

module.exports = {
  initialize,
  model: Schedule
}