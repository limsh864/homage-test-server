const Sequelize = require('sequelize')
const sequelize = require('../../libs/sequelize')

const CareRecipient = sequelize.define('CareRecipient', {

  id : { type: Sequelize.UUID, primaryKey: true },
  email : { type: Sequelize.STRING, validate: { isEmail: true }, allowNull: false },
  fullname : { type: Sequelize.STRING, allowNull: false },
  mobile : { type: Sequelize.STRING, allowNull: false },
  picture : { type: Sequelize.STRING, allowNull: true }
}, {
  paranoid: true
})

function initialize() {
    return CareRecipient
}

module.exports = {
  initialize,
  model: CareRecipient
}