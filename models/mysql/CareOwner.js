const Sequelize = require('sequelize')
const sequelize = require('../../libs/sequelize')

const CareOwner = sequelize.define('CareOwner', {

  id : { type: Sequelize.UUID, primaryKey: true },
  email : { type: Sequelize.STRING, validate: { isEmail: true }, allowNull: false },
  fullname : { type: Sequelize.STRING, allowNull: false },
  mobile : { type: Sequelize.STRING, allowNull: false },
  picture : { type: Sequelize.STRING, allowNull: true }
}, {
  paranoid: true
})

function initialize({ CareRecipient }) {
  CareOwner.hasMany(CareRecipient, { as : 'careRecipient' })
    return CareOwner
}

module.exports = {
  initialize,
  model: CareOwner
}