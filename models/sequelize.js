const _ = require('lodash')
const fs = require('fs')

function loadModels(modelDir) {
  const modelNames = fs.readdirSync(`${__dirname}/${modelDir}`).filter((file) => {
    // include only js files in target directory,
    // exclude index.js
    return /^(.+)\.js$/.test(file) && file !== 'index.js'
  }).map((file) => file.slice(0, -3))

  const loaders = {}
  const models = {}

  _.each(modelNames, (name) => {
    const key = _.upperFirst(name)

    loaders[key] = require(`./${modelDir}/${name}`)
    models[key] = loaders[key].model
  })

  _.each(loaders, (loader, name) => {
    // initialize each Model loader
    models[name] = loader.initialize(models)
  })

  return models
}

module.exports = loadModels('mysql')
